package com.klr.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Row.MissingCellPolicy;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.bson.Document;

public class ExcelUtils {
	public static void main(String[] args) throws IOException {
		System.out.print("Enter the File Path: ");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String filePath = "";
		try {
//			while ((filePath = reader.readLine()) != null) {
//				if (filePath.equalsIgnoreCase("quit")) {
//					break;
//				}
//				System.out.println("Filepath : " + filePath);
//			}
			filePath = reader.readLine();
			System.out.println("Filepath : " + filePath);
			parseExcelToJson(filePath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static ArrayList<Map<String, Object>> parseExcelToJson(String filePath) throws Exception {
		List<String> keys = new ArrayList();
		Map<String, Object> tempObj = null;
		ArrayList<Map<String, Object>> finalList = new ArrayList();
		Workbook workbook = WorkbookFactory.create(FileUtils.getFile(filePath));
		System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");

		workbook.forEach(sheet -> {
			System.out.println("=> " + sheet.getSheetName());
		});
		Sheet sheet = workbook.getSheetAt(0);
		DataFormatter dataFormatter = new DataFormatter();
		FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
		dataFormatter.addFormat("mm/dd/yy", new java.text.SimpleDateFormat("yyyy-MM-dd"));
		dataFormatter.addFormat("m/d/yy", new java.text.SimpleDateFormat("yyyy-MM-dd"));
		int i = 0;
		for (int rowNum = sheet.getFirstRowNum(); rowNum < sheet.getLastRowNum(); rowNum++) {
			Row row = sheet.getRow(rowNum);
			if (row == null) {
				continue;
			}
			i = 0;
			tempObj = new Document();
			for (int cn = 0; cn < row.getLastCellNum(); cn++) {
				Cell cell = row.getCell(cn, MissingCellPolicy.RETURN_BLANK_AS_NULL);
				if (cell == null) {
					tempObj.put(keys.get(i), null);
				} else {
					String cellValue = dataFormatter.formatCellValue(cell, evaluator);
					if (row.getRowNum() == 0) {
						keys.add(cellValue);
					} else {
						tempObj.put(keys.get(i), cellValue);
					}
				}
				i++;
			}
			if (!tempObj.isEmpty()) {
				finalList.add(tempObj);
			}
		}
		workbook.close();
		System.out.println(finalList);
		return finalList;
	}
}
