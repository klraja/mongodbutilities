package com.klr.main;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class MongoDBCopy {
	
	public static void main(String[] args) {

		//Mongo Client without Auth
		MongoClient mongoClient1 = new MongoClient( "localhost" , 27017 ); 
		MongoDatabase database1 = mongoClient1.getDatabase("US_ONLINE");
		MongoCollection<Document> collection1 = database1.getCollection("mlr_views_31_5"); 
		
		//Mongo Client with Auth
		MongoClientURI uri = new MongoClientURI("mongodb://<USERNAME>:<PASSWORD>@<MONGOHOST>:<POST>");
		MongoClient mongoClient = new MongoClient(uri);
		MongoDatabase database = mongoClient.getDatabase("US_ONLINE");
		MongoCollection<Document> collection = database.getCollection("mlr_views");

		FindIterable<Document> iterDoc = collection.find();
		int i = 1;

		Iterator it = iterDoc.iterator();
		List<Document> docList = new ArrayList();
		while (it.hasNext()) {
			docList.add((Document) it.next());
			i++;
		}
		collection1.insertMany(docList);
	}
}
