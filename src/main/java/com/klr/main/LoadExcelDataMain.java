package com.klr.main;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Map;

import org.bson.Document;

import com.klr.utilities.ExcelUtils;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class LoadExcelDataMain {
	public static void main(String[] args) {
		System.out.print("Enter the File Path: ");
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String filePath = "";
		ArrayList<Map<String, Object>> dataSourceList = new ArrayList();
		try {
			filePath = reader.readLine();
			System.out.println("Filepath : " + filePath);
			dataSourceList = ExcelUtils.parseExcelToJson(filePath);
		} catch (Exception e) {
			e.printStackTrace();
		}
//		MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
//		MongoClientURI uri = new MongoClientURI("mongodb://<USERNAME>:<PASSWORD>@<MONGOHOST>:<POST>");
		MongoClient mongoClient;
		MongoDatabase database;
		MongoCollection<Document> collection;
		for (Map<String, Object> data: dataSourceList) {
			mongoClient = new MongoClient( "localhost" , 27017 );
			//mongoClient = new MongoClient(uri);
			database = mongoClient.getDatabase("US_ONLINE");
			collection = database.getCollection("mlr_crp_metaData_31_5");
			collection.insertOne((Document) data);
			mongoClient.close();
		}
	}
}
